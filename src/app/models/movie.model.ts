export class Movie {
    id: number;
    title: string;
    description: string;
    rating: string;
    duration: string;
    genre: string[];
    releasedDate: string;
    trailerLink: string;
    posterFileName: string;

    constructor(id: number, title: string, description: string, rating: string, duration: string, genre: string[], releasedDate: string, trailerLink: string, posterFileName: string) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.rating = rating;
        this.duration = duration;
        this.genre = genre;
        this.releasedDate = releasedDate;
        this.trailerLink = trailerLink;
        this.posterFileName = posterFileName;
    }
}