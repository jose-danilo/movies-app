import { Component } from '@angular/core';
import { Movie } from 'src/app/models/movie.model';
import { MoviesService } from 'src/app/services/movies.service';

@Component({
  selector: 'app-my-list',
  templateUrl: './my-list.component.html',
  styleUrls: ['./my-list.component.scss']
})
export class MyListComponent {

  public myListMoviesIds: number[] = [];
  public myListMovies: Movie[] = [];

  constructor(private moviesService: MoviesService) { }

  ngOnInit(): void {
    this.myListMoviesIds = this.moviesService.getMyList();
    this.getMyListDetails();
  }

  private getMyListDetails(): void {
    for (let movieId of this.myListMoviesIds) {
      this.moviesService.getMovieById(movieId).subscribe(movie => {
        if (movie) this.myListMovies.push(movie);
      });
    }
  }
}
