import { Component, Input } from '@angular/core';
import { Movie } from 'src/app/models/movie.model';

@Component({
  selector: 'app-list-movies',
  templateUrl: './list-movies.component.html',
  styleUrls: ['./list-movies.component.scss']
})
export class ListMoviesComponent {
  @Input({ required: true }) movies!: Movie[];

  public activeFilter: string = '';

  public sortByTitle(): void {
    this.activeFilter = 'title';
    this.movies.sort((a, b) => {
      if (a.title < b.title) {
        return -1;
      } else if (a.title > b.title) {
        return 1;
      } else {
        return 0;
      }
    })
  }

  public sortByDateDescending(): void {
    this.activeFilter = 'date';
    this.movies.sort((a, b) => {
      const dateA = new Date(a.releasedDate);
      const dateB = new Date(b.releasedDate);
      return dateB.getTime() - dateA.getTime();
    })
  }
}
