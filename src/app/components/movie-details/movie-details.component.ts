import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Movie } from 'src/app/models/movie.model';
import { MoviesService } from 'src/app/services/movies.service';

@Component({
  selector: 'app-movie-details',
  templateUrl: './movie-details.component.html',
  styleUrls: ['./movie-details.component.scss']
})
export class MovieDetailsComponent {

  public movie!: Movie;
  public isMovieInMyList: boolean = false;

  constructor(private route: ActivatedRoute, private moviesService: MoviesService) {
  }

  ngOnInit(): void {
    let movieId: number = 0;
    this.route.params.subscribe(param => {
      movieId = parseInt(param['id']);
    })

    this.moviesService.getMovieById(movieId).subscribe(movie => {
      if (movie) this.movie = movie;
    });

    this.isMovieInMyList = this.isMovieAlreadySaved();
  }

  public isMovieAlreadySaved(): boolean {
    return this.moviesService.isMovieInMyList(this.movie);
  }

  public constructYoutubeEmbeddedUrl(url: string): string {
    return url.replace("/watch?v=", "/embed/");
  }

  public saveToMyList(): void {
    this.moviesService.saveToMyList(this.movie);
    this.isMovieInMyList = this.isMovieAlreadySaved();
  }

  public removeFromMyList(): void {
    this.moviesService.removeFromMyList(this.movie);
    this.isMovieInMyList = this.isMovieAlreadySaved();
  }
}
