import { Component, Input } from '@angular/core';
import { Movie } from 'src/app/models/movie.model';
import { MoviesService } from 'src/app/services/movies.service';

@Component({
  selector: 'app-movie-card',
  templateUrl: './movie-card.component.html',
  styleUrls: ['./movie-card.component.scss']
})
export class MovieCardComponent {
  @Input({ required: true }) movie!: Movie;

  constructor(private moviesService: MoviesService) { }

  public isMovieInMyList(movie: Movie): boolean {
    return this.moviesService.isMovieInMyList(movie);
  }
}
