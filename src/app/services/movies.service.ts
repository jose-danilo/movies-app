import { Injectable } from '@angular/core';
import { Movie } from '../models/movie.model';
import { Observable, of } from 'rxjs';
import { movies } from './movies-data';

@Injectable({
  providedIn: 'root'
})
export class MoviesService {

  private readonly MY_LIST_NAME: string = "myList";

  constructor() { }

  // Mock observables, ideally this data would come from API calls

  public getMovies(): Observable<Movie[]> {
    return of(movies);
  }

  public getMovieById(id: number): Observable<Movie | undefined> {
    let movie: Movie | undefined = movies.find(element => element.id === id);
    return of(movie);
  }

  public getMyList(): number[] {
    let myListMoviesIds: number[] = [];
    let myListStorage = window.localStorage.getItem(this.MY_LIST_NAME);
    if (myListStorage != null) {
      myListMoviesIds = JSON.parse(myListStorage);
    }
    return myListMoviesIds;
  }

  public isMovieInMyList(movie: Movie): boolean {
    let myListMoviesIds: number[] = this.getMyList();
    const index = myListMoviesIds.indexOf(movie.id);
    return index > -1 ? true : false;
  }

  public saveToMyList(movie: Movie): void {
    let myListMoviesIds: number[] = this.getMyList();
    myListMoviesIds.push(movie.id);
    window.localStorage.setItem(this.MY_LIST_NAME, JSON.stringify(myListMoviesIds));
  }

  public removeFromMyList(movie: Movie): void {
    let myListMoviesIds: number[] = this.getMyList();
    const index = myListMoviesIds.indexOf(movie.id);
    if (index > -1) {
      myListMoviesIds.splice(index, 1);
    }
    window.localStorage.setItem(this.MY_LIST_NAME, JSON.stringify(myListMoviesIds));
  }
}
